/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.satit.robootwalkproject;

/**
 *
 * @author Satit Wapeetao
 */
public class Robot {

    private int x, y;
    private char symbol;
    private Tablemap map;

    public Robot(int x, int y, char symbol, Tablemap map) {
        this.x = x;
        this.y = y;
        this.symbol = symbol;
        this.map = map;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public char getSymbol() {
        return symbol;
    }

    public boolean walk(char direction) {
        switch (direction) {
            case 'N':
            case 'w':
                if (map.inMap(x, y - 1)) {
                    y = y - 1;
                } else {
                    return false;
                }
                break;
            case 'S':
            case 's':
                if (map.inMap(x, y + 1)) {
                    y = y + 1;
                } else {
                    return false;
                }
                break;
            case 'E':
            case 'd':
                if (map.inMap(x + 1, y)) {
                    x = x + 1;
                } else {
                    return false;
                }
                break;
            case 'W':
            case 'a':
                if (map.inMap(x - 1, y - 1)) {
                    x = x - 1;
                } else {
                    return false;
                }
                break;
            default:
                return false;

        }
        if(map.isBomb(x, y)){
            System.out.println("Founded Bomb!!!!");
        }
        return true;
    }

    public boolean isOn(int x, int y) {
        return this.x == x && this.y == y;
    }

}
