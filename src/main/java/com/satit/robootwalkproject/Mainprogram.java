/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.satit.robootwalkproject;

import java.util.Scanner;

/**
 *
 * @author Satit Wapeetao
 */
public class Mainprogram {
    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        Tablemap map = new Tablemap(10,10);
        Robot robot = new Robot(2,2,'x',map);
        Bomb bomb = new Bomb (5,5);
        map.setRobot(robot);
        map.setBomb(bomb);
        while(true){
            map.showMap();
            char direction= Inputdirection(kb);
            if(direction=='q'){
                ByeBye();
                break;
            }
           robot.walk(direction);
        }
        
    }

    private static void ByeBye() {
        System.out.println("Bye bro");
    }

    private static char Inputdirection(Scanner kb) {
        String str= kb.next();
        char direction = str.charAt(0);
        return direction;
    }
}
