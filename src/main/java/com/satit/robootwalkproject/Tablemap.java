/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.satit.robootwalkproject;

/**
 *
 * @author Satit Wapeetao
 */
class Tablemap {

    private int width, height;
    private Robot robot;
    private Bomb bomb;

    public Tablemap(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public void setRobot(Robot robot) {
        this.robot = robot;
    }

    public void setBomb(Bomb bomb) {
        this.bomb = bomb;
    }

    public void showMap() {
        showTitle();
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                if (robot.isOn(x, y)) {
                    showRobot();          
            }else if(bomb.isOn(x, y)){
                    showbomb();
                }else{
                    showCell();
            }
        }
            shownewline();
    }
}

    private void showTitle() {
        System.out.println("Map");
    }

    private void shownewline() {
        System.out.println("");
    }

    private void showCell() {
        System.out.print("-");
    }

    private void showbomb() {
        System.out.print(bomb.getsymbol());
    }

private void showRobot() {
        System.out.print(robot.getSymbol());
    }

    public boolean inMap(int x, int y) {
        return (x >= 0 && x < width) && (y >= 0 && y < height);
    }

    public boolean isBomb(int x, int y) {
        return bomb.isOn(x, y);
    }
}
